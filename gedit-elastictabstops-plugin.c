/*
 * gedit-elastictabstops-plugin.c
 * This version released: 2007-09-16 (first release)
 * This version released: 2020-03-28
 *
 * Copyright (C) 2007 Nick Gravgaard
 * Copyright (C) 2021 Dorota Czaplejewicz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "gedit-elastictabstops-plugin.h"

#include <glib/gi18n-lib.h>
#include <gmodule.h>

#include <gtk/gtk.h>

#include <pluma/pluma-debug.h>
#include <pluma/pluma-document.h>
#include <pluma/pluma-window.h>

#ifdef PLUMA_1_25
#include <pluma/pluma-window-activatable.h>
#else
#include <libpeas/peas-activatable.h>
#define PLUMA_TYPE_WINDOW_ACTIVATABLE PEAS_TYPE_ACTIVATABLE
#define PlumaWindowActivatableInterface PeasActivatableInterface
#define PlumaWindowActivatable PeasActivatable
#endif
//#include <gconf/gconf-client.h>


#define GeditDocument PlumaDocument
#define GeditTab PlumaTab
#define GeditView PlumaView
#define GeditWindow PlumaWindow

#define GEDIT_VIEW PLUMA_VIEW
#define GEDIT_DOCUMENT PLUMA_DOCUMENT

#define gedit_debug pluma_debug
#define gedit_tab_get_view pluma_tab_get_view
#define gedit_window_get_active_document pluma_window_get_active_document
#define gedit_window_get_active_view pluma_window_get_active_view
#define gedit_window_get_documents pluma_window_get_documents
#define gedit_window_get_views pluma_window_get_views

#define GEDIT_ELASTICTABSTOPS_PLUGIN_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN, GeditElasticTabstopsPluginPrivate))

enum {
	PROP_0,
	PROP_OBJECT
};


typedef struct
{
	int text_width_pix;
	int *widest_width_pix;
	gboolean ends_in_tab;
} et_tabstop;

typedef struct
{
	int num_tabs;
} et_line;

typedef enum
{
	BACKWARDS,
	FORWARDS
} direction;

// use the following commands to set the minimum width and padding width values
// gconftool-2 --type int --set /apps/gedit-2/plugins/elastictabstops/minimumwidth 40
// gconftool-2 --type int --set /apps/gedit-2/plugins/elastictabstops/paddingwidth 16

#define MINIMUM_WIDTH_KEY "/apps/gedit-2/plugins/elastictabstops/minimumwidth"
#define PADDING_WIDTH_KEY "/apps/gedit-2/plugins/elastictabstops/paddingwidth"

// by default, tabstops are at least 40 pixels plus 16 pixels of padding
#define MINIMUM_WIDTH_DEFAULT 40
#define PADDING_WIDTH_DEFAULT 16

static int tab_width_minimum = 0;
static int tab_width_padding = 0;

static int get_text_width(GeditView *view, GtkTextIter *start, GtkTextIter *end)
{
	GdkRectangle start_rect, end_rect;

	gtk_text_view_get_iter_location(GTK_TEXT_VIEW(&view->view.parent), start, &start_rect);
	gtk_text_view_get_iter_location(GTK_TEXT_VIEW(&view->view.parent), end, &end_rect);

	return end_rect.x - start_rect.x;
}

static int calc_tab_width(int text_width_in_tab)
{
	if (text_width_in_tab < tab_width_minimum)
	{
		text_width_in_tab = tab_width_minimum;
	}
	return text_width_in_tab + tab_width_padding;
}

static gboolean change_line(GtkTextIter *location, direction which_dir)
{
	if (which_dir == FORWARDS)
	{
		return gtk_text_iter_forward_line(location);
	}
	else
	{
		return gtk_text_iter_backward_line(location);
	}
}

// returns the max number of tabs found, and sets location to the first line it finds with no tabs in
static int get_block_boundary(GtkTextBuffer *textbuffer, GtkTextIter *location, direction which_dir)
{
	GtkTextIter current_pos;
	int max_tabs = 0;
	gboolean orig_line = TRUE;

	gtk_text_iter_set_line_offset(location, 0);
	do
	{
		int tabs_on_line = 0;
		gunichar current_char;
		gboolean current_char_ends_line;

		current_pos = *location;
		current_char = gtk_text_iter_get_char(&current_pos);
		current_char_ends_line = gtk_text_iter_ends_line(&current_pos);

		while (current_char != '\0' && current_char_ends_line == FALSE)
		{
			if (current_char == '\t')
			{
				tabs_on_line++;
				if (tabs_on_line > max_tabs)
				{
					max_tabs = tabs_on_line;
				}
			}
			gtk_text_iter_forward_char(&current_pos);
			current_char = gtk_text_iter_get_char(&current_pos);
			current_char_ends_line = gtk_text_iter_ends_line(&current_pos);
		}
		if (tabs_on_line == 0 && orig_line == FALSE)
		{
			return max_tabs;
		}
		orig_line = FALSE;
	} while (change_line(location, which_dir));
	return max_tabs;
}

// returns the max number of tabs found between the start and end iters
static int get_nof_tabs_between(GtkTextBuffer *textbuffer, GtkTextIter *start, GtkTextIter *end)
{
	GtkTextIter current_pos = *start;
	int max_tabs = 0;

	gtk_text_iter_set_line_offset(&current_pos, 0);
	do
	{
		int tabs_on_line = 0;
		gunichar current_char;
		gboolean current_char_ends_line;

		current_char = gtk_text_iter_get_char(&current_pos);
		current_char_ends_line = gtk_text_iter_ends_line(&current_pos);

		while (current_char != '\0' && current_char_ends_line == FALSE)
		{
			if (current_char == '\t')
			{
				tabs_on_line++;
				if (tabs_on_line > max_tabs)
				{
					max_tabs = tabs_on_line;
				}
			}
			gtk_text_iter_forward_char(&current_pos);
			current_char = gtk_text_iter_get_char(&current_pos);
			current_char_ends_line = gtk_text_iter_ends_line(&current_pos);
		}
	} while (change_line(&current_pos, FORWARDS) && gtk_text_iter_compare(&current_pos, end) < 0);
	return max_tabs;
}

static void stretch_tabstops(GtkTextBuffer *textbuffer, GeditView *view, int block_start_linenum, int block_nof_lines, int max_tabs)
{
	int l, t;
	et_line lines[block_nof_lines];
	et_tabstop grid[block_nof_lines][max_tabs];

	memset(lines, 0, sizeof(lines));
	memset(grid, 0, sizeof(grid));

	// get width of text in cells
	for (l = 0; l < block_nof_lines; l++) // for each line
	{
		GtkTextIter current_pos, cell_start;
		int text_width_in_tab = 0;
		int current_line_num = block_start_linenum + l;
		int current_tab_num = 0;
		gunichar current_char;
		gboolean current_char_ends_line;
		gboolean cell_empty = TRUE;

		gtk_text_buffer_get_iter_at_line(textbuffer, &current_pos, current_line_num);
		cell_start = current_pos;
		current_char = gtk_text_iter_get_char(&current_pos);
		current_char_ends_line = gtk_text_iter_ends_line(&current_pos);
		// maybe change this to search forwards for tabs/newlines using gtk_text_iter_forward_find_char
		// see http://www.bravegnu.org/gtktext/x370.html

		while (current_char != '\0')
		{
			if (current_char_ends_line == TRUE)
			{
				grid[l][current_tab_num].ends_in_tab = FALSE;
				text_width_in_tab = 0;
				break;
			}
			else if (current_char == '\t')
			{
				if (cell_empty == FALSE)
				{
					text_width_in_tab = get_text_width(view, &cell_start, &current_pos);
				}
				grid[l][current_tab_num].ends_in_tab = TRUE;
				grid[l][current_tab_num].text_width_pix = calc_tab_width(text_width_in_tab);
				current_tab_num++;
				lines[l].num_tabs++;
				text_width_in_tab = 0;
				cell_empty = TRUE;
			}
			else
			{
				if (cell_empty == TRUE)
				{
					cell_start = current_pos;
					cell_empty = FALSE;
				}
			}
			gtk_text_iter_forward_char(&current_pos);
			current_char = gtk_text_iter_get_char(&current_pos);
			current_char_ends_line = gtk_text_iter_ends_line(&current_pos);
		}
	}

	// find columns blocks and stretch to fit the widest cell
	for (t = 0; t < max_tabs; t++) // for each column
	{
		gboolean starting_new_block = TRUE;
		int first_line_in_block = 0;
		int max_width = 0;
		for (l = 0; l < block_nof_lines; l++) // for each line
		{
			if (starting_new_block == TRUE)
			{
				starting_new_block = FALSE;
				first_line_in_block = l;
				max_width = 0;
			}
			if (grid[l][t].ends_in_tab == TRUE)
			{
				grid[l][t].widest_width_pix = &(grid[first_line_in_block][t].text_width_pix); // point widestWidthPix at first 
				if (grid[l][t].text_width_pix > max_width)
				{
					max_width = grid[l][t].text_width_pix;
					grid[first_line_in_block][t].text_width_pix = max_width;
				}
			}
			else // end column block
			{
				starting_new_block = TRUE;
			}
		}
	}

	// set tabstops
	for (l = 0; l < block_nof_lines; l++) // for each line
	{
		GtkTextIter line_start, line_end;
		GtkTextTag *tag;
		PangoTabArray *tab_array;
		int current_line_num = block_start_linenum + l;
		int acc_tabstop = 0;
		
		tab_array = pango_tab_array_new(lines[l].num_tabs, TRUE);

		for (t = 0; t < lines[l].num_tabs; t++)
		{
			if (grid[l][t].widest_width_pix != NULL)
			{
				acc_tabstop += *(grid[l][t].widest_width_pix);
				pango_tab_array_set_tab(tab_array, t, PANGO_TAB_LEFT, acc_tabstop);
			}
		}

		tag = gtk_text_buffer_create_tag(textbuffer, NULL, "tabs", tab_array, NULL);
	
		/* Apply word_wrap tag to whole buffer */
		/* gtk_text_buffer_get_bounds(textbuffer, &start, &end);*/
	
		gtk_text_buffer_get_iter_at_line(textbuffer, &line_start, current_line_num);
	
		line_end = line_start;
	
		if (gtk_text_iter_ends_line(&line_end) == FALSE)
		{
			gtk_text_iter_forward_to_line_end(&line_end);
		}
	
		//gtk_text_buffer_remove_all_tags(textbuffer, &line_start, &line_end); // is this necessary?
		gtk_text_buffer_apply_tag(textbuffer, tag, &line_start, &line_end);
	
		pango_tab_array_free(tab_array);
	}
}

static void elastictabstops_onmodify(GtkTextBuffer *textbuffer, GtkTextIter *start, GtkTextIter *end, GeditView *view)
{
	GtkTextIter block_start_iter, block_end_iter;
	int block_start_linenum, block_end_linenum, block_nof_lines;
	int max_tabs_backwards, max_tabs_between, max_tabs_forwards, max_tabs;

	block_start_iter = *start;
	block_end_iter = *end;

	max_tabs_between = get_nof_tabs_between(textbuffer, &block_start_iter, &block_end_iter);
	max_tabs_backwards = get_block_boundary(textbuffer, &block_start_iter, BACKWARDS);
	max_tabs_forwards = get_block_boundary(textbuffer, &block_end_iter, FORWARDS);
	max_tabs = MAX(MAX(max_tabs_between, max_tabs_backwards), max_tabs_forwards);

	block_start_linenum = gtk_text_iter_get_line(&block_start_iter);
	block_end_linenum = gtk_text_iter_get_line(&block_end_iter);
	block_nof_lines = (block_end_linenum - block_start_linenum) + 1;

	stretch_tabstops(textbuffer, view, block_start_linenum, block_nof_lines, max_tabs);
}

struct _GeditElasticTabstopsPluginPrivate
{
	GtkWidget *window;
};


//static void gedit_elastictabstops_plugin_class_init(GeditElasticTabstopsPluginClass *klass);
static void
pluma_window_activatable_iface_init (PlumaWindowActivatableInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (GeditElasticTabstopsPlugin,
                                gedit_elastictabstops_plugin,
                                PEAS_TYPE_EXTENSION_BASE,
                                0,
                                G_ADD_PRIVATE_DYNAMIC (GeditElasticTabstopsPlugin)
                                G_IMPLEMENT_INTERFACE_DYNAMIC (PLUMA_TYPE_WINDOW_ACTIVATABLE,
                                                               pluma_window_activatable_iface_init))

G_MODULE_EXPORT void
peas_register_types (PeasObjectModule *module)
{
	gedit_elastictabstops_plugin_register_type (G_TYPE_MODULE (module));

	peas_object_module_register_extension_type (module,
	                                            PEAS_TYPE_ACTIVATABLE,
	                                            GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN);
}

static void gedit_elastictabstops_plugin_finalize(GObject *object)
{
/*
	GeditElasticTabstopsPlugin *plugin = GEDIT_ELASTICTABSTOPS_PLUGIN(object);
*/
	pluma_debug_message(DEBUG_PLUGINS, "GeditElasticTabstopsPlugin finalising");

	G_OBJECT_CLASS(gedit_elastictabstops_plugin_parent_class)->finalize(object);
}

static void
gedit_elastictabstops_plugin_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
	GeditElasticTabstopsPlugin *plugin = GEDIT_ELASTICTABSTOPS_PLUGIN (object);

	switch (prop_id)
	{
		case PROP_OBJECT:
			g_value_set_object (value, plugin->priv->window);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
gedit_elastictabstops_plugin_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
	GeditElasticTabstopsPlugin *plugin = GEDIT_ELASTICTABSTOPS_PLUGIN (object);

	switch (prop_id)
	{
		case PROP_OBJECT:
			plugin->priv->window = GTK_WIDGET (g_value_dup_object (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void gedit_elastictabstops_plugin_class_init(GeditElasticTabstopsPluginClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

    object_class->finalize = gedit_elastictabstops_plugin_finalize;
	object_class->set_property = gedit_elastictabstops_plugin_set_property;
	object_class->get_property = gedit_elastictabstops_plugin_get_property;

	g_object_class_override_property (object_class, PROP_OBJECT, "object");
}


static void
gedit_elastictabstops_plugin_class_finalize (GeditElasticTabstopsPluginClass *klass)
{
	/* dummy function - used by G_DEFINE_DYNAMIC_TYPE_EXTENDED */
}

static void gedit_elastictabstops_plugin_init(GeditElasticTabstopsPlugin *plugin)
{
	plugin->priv = gedit_elastictabstops_plugin_get_instance_private (plugin);

	pluma_debug_message(DEBUG_PLUGINS, "GeditElasticTabstopsPlugin initialising");
}


static void loaded_cb(	GeditDocument *document,
	const GError  *error, GeditView *view);

static gboolean insert_text_cb(	GtkTextBuffer *textbuffer, GtkTextIter *location,
	gchar *text, gint len, GeditView *view);

static gboolean delete_range_cb(	GtkTextBuffer *textbuffer,
	GtkTextIter *start, GtkTextIter *end, GeditView *view);

static void connect_handlers(GeditView *view)
{
	GtkTextBuffer *doc;
	doc = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));

	g_signal_connect(	doc, "loaded",
		G_CALLBACK(loaded_cb), view);

	g_signal_connect_after(	doc, "insert-text",
		G_CALLBACK(insert_text_cb), view);

	g_signal_connect_after(	doc, "delete-range",
		G_CALLBACK(delete_range_cb), view);
}

static void loaded_cb(	GeditDocument *document,
	const GError  *error, GeditView *view)
{
	GtkTextIter start, end;
	gtk_text_buffer_get_bounds(&document->buffer.parent_instance, &start, &end);
	elastictabstops_onmodify(&document->buffer.parent_instance, &start, &end, view);
}

static gboolean insert_text_cb(	GtkTextBuffer *textbuffer, GtkTextIter *location,
	gchar *text, gint len, GeditView *view)
{
	GtkTextIter start, end;
	start = end = *location;
	gtk_text_iter_backward_chars(&start, len);
	elastictabstops_onmodify(textbuffer, &start, &end, view);
	return FALSE;
}

static gboolean delete_range_cb(	GtkTextBuffer *textbuffer,
	GtkTextIter *start, GtkTextIter *end, GeditView *view)
{
	elastictabstops_onmodify(textbuffer, start, end, view);
	return FALSE;
}

static void tab_added_cb(	GeditWindow *window,
	GeditTab *tab, gpointer user_data)
{
	connect_handlers(gedit_tab_get_view(tab));
}

static void impl_activate(	PlumaWindowActivatable *activatable)
{
	GeditDocument *doc;
	PlumaView *view;
	GtkTextBuffer *textbuffer;
	GeditElasticTabstopsPlugin *plugin;
	GeditElasticTabstopsPluginPrivate *data;
	PlumaWindow *window;

	gedit_debug(DEBUG_PLUGINS);

	plugin = GEDIT_ELASTICTABSTOPS_PLUGIN (activatable);
	data = plugin->priv;
	window = PLUMA_WINDOW (data->window);

	doc = gedit_window_get_active_document(window);
	view = gedit_window_get_active_view(window);
	textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));

/*	GConfClient *client= gconf_client_get_default();
	tab_width_minimum = gconf_client_get_int(client, MINIMUM_WIDTH_KEY, NULL);
	if (tab_width_minimum <= 0)
	{
		tab_width_minimum = MINIMUM_WIDTH_DEFAULT;
	}
	tab_width_padding = gconf_client_get_int(client, PADDING_WIDTH_KEY, NULL);
	if (tab_width_padding <= 0)
	{
		tab_width_padding = PADDING_WIDTH_DEFAULT;
	}
	g_object_unref(client);
	*/
    tab_width_minimum = MINIMUM_WIDTH_DEFAULT;
	tab_width_padding = PADDING_WIDTH_DEFAULT;
	// see mode-line plugin
	GList *views;
	GList *l;

	views = gedit_window_get_views(window);
	for (l = views; l != NULL; l = l->next)
	{
		connect_handlers(GEDIT_VIEW(l->data));
	}
	g_list_free(views);
		
	g_signal_connect(window, "tab-added",
		G_CALLBACK(tab_added_cb), NULL);
}

static void impl_deactivate(PlumaWindowActivatable *activatable)
{
	PlumaWindow *window;
	GeditElasticTabstopsPluginPrivate *data;
	gedit_debug(DEBUG_PLUGINS);
	
	// un-register callbacks - see mode-line plugin
	GList *doc;
	GList *l;
	data = GEDIT_ELASTICTABSTOPS_PLUGIN (activatable)->priv;
	window = PLUMA_WINDOW (data->window);
	doc = gedit_window_get_documents(window);
	for (l = doc; l != NULL; l = l->next)
	{
		g_signal_handlers_disconnect_by_func(GEDIT_DOCUMENT(l->data),
			loaded_cb, NULL);
		g_signal_handlers_disconnect_by_func(GEDIT_DOCUMENT(l->data),
			insert_text_cb, NULL);
		g_signal_handlers_disconnect_by_func(GEDIT_DOCUMENT(l->data),
			delete_range_cb, NULL);
	}
	g_list_free(doc);
	
	g_signal_handlers_disconnect_by_func(window, tab_added_cb, NULL);
}

static void impl_update_ui(PlumaWindowActivatable *activatable)
{
	gedit_debug(DEBUG_PLUGINS);
}

static void
pluma_window_activatable_iface_init (PlumaWindowActivatableInterface *iface)
{
	iface->activate = impl_activate;
	iface->deactivate = impl_deactivate;
	iface->update_state = impl_update_ui;
}
