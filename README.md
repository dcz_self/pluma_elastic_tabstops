Pluma Elastic Tabstops plugin
=================

This plugin provides [elastic tabstops](http://nickgravgaard.com/elastictabstops) for [Pluma](https://github.com/mate-desktop/pluma/), the [Mate desktop](http://www.mate-desktop.org/) text editor.

What are elastic tabstops?
-------------------------------

![Demonstration](https://nickgravgaard.com/elastic-tabstops/images/logo.gif)

Elastic tabstops are a different way to treat tab characters.

Traditionally, tabs are used to align text to a certain position on the screen. The elastic version will align them to each other, reflecting the *meaning* rather than the *position* of indentation.

Installation
-------------

Download the plugin archive from: https://gitlab.com/dcz_self/pluma_elastic_tabstops/-/releases

Then extract its contents into `~/.local/share/pluma/plugins/`

Building
---------

For up-to-date build instructions, see the file `.gitab-ci.yml`.

Pluma version required is 1.24. The plugin API changes in 1.25, so this may not work.

```
mkdir ../build
meson ../build .
cd ../build
ninja
mkdir -p ~/.local/share/pluma/plugins/tabstops
cp ./tabstops.plugin ~/.local/share/pluma/plugins/tabstops/
cp ./libelastictabstops.so ~/.local/share/pluma/plugins/tabstops/
```

Copying
---------

This sotware can be distributed under the terms of GPL version 3 or any later version, at your leisure. (See the file gpl-3.0.md.)
