/*
 * gedit-elastictabstops-plugin.h
 * This version released: 2007-09-16 (first release)
 * This version released: 2020-03-28
 *
 * Copyright (C) 2007 Nick Gravgaard
 * Copyright (C) 2021 Dorota Czaplejewicz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GEDIT_ELASTICTABSTOPS_PLUGIN_H__
#define __GEDIT_ELASTICTABSTOPS_PLUGIN_H__

#include <glib.h>
#include <glib-object.h>
#include <libpeas/peas-extension-base.h>
#include <libpeas/peas-object-module.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN		(gedit_elastictabstops_plugin_get_type ())
#define GEDIT_ELASTICTABSTOPS_PLUGIN(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN, GeditElasticTabstopsPlugin))
#define GEDIT_ELASTICTABSTOPS_PLUGIN_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN, GeditElasticTabstopsPluginClass))
#define GEDIT_IS_ELASTICTABSTOPS_PLUGIN(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN))
#define GEDIT_IS_ELASTICTABSTOPS_PLUGIN_CLASS(k)		(G_TYPE_CHECK_CLASS_TYPE ((k), GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN))
#define GEDIT_ELASTICTABSTOPS_PLUGIN_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GEDIT_TYPE_ELASTICTABSTOPS_PLUGIN, GeditElasticTabstopsPluginClass))

/* Private structure type */
typedef struct _GeditElasticTabstopsPluginPrivate	GeditElasticTabstopsPluginPrivate;

/*
 * Main object structure
 */
typedef struct _GeditElasticTabstopsPlugin		GeditElasticTabstopsPlugin;

struct _GeditElasticTabstopsPlugin
{
	PeasExtensionBase parent_instance;

	/*< private >*/
	GeditElasticTabstopsPluginPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _GeditElasticTabstopsPluginClass	GeditElasticTabstopsPluginClass;

struct _GeditElasticTabstopsPluginClass
{
	PeasExtensionBaseClass parent_class;
};

/*
 * Public methods
 */
GType	gedit_elastictabstops_plugin_get_type		(void) G_GNUC_CONST;

/* All the plugins must implement this function */
G_MODULE_EXPORT void peas_register_types (PeasObjectModule *module);

G_END_DECLS

#endif /* __GEDIT_ELASTICTABSTOPS_PLUGIN_H__ */

